﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObeliskMovement : MonoBehaviour {

    public GameObject obelisco;
    public float turnSpeed = 22.5f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        obelisco.transform.Rotate(0, turnSpeed, 0, Space.Self);
    }
}
